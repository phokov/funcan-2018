TEX_PATH := ./tex
ASSETS_PATH := ./assets
TEX_FILES := $(wildcard $(TEX_PATH)/*.tex)
PRECOMPILED_PDF_FILES := $(wildcard $(ASSETS_PATH)/*.pdf)
PDF_FILES_TO_COMPILE := $(patsubst $(TEX_PATH)/%.tex,%.pdf,$(TEX_FILES))

BUILD_DIR := ./build
PDFLATEX_FLAGS := -halt-on-error # -output-directory $(BUILD_DIR)

OUTPUT_PDF := funcan-2018.pdf

all: $(OUTPUT_PDF)

$(OUTPUT_PDF): $(PDF_FILES_TO_COMPILE) .copy_assets
	rm -f $@
	pdftk *.pdf cat output $@

.copy_assets: $(PRECOMPILED_PDF_FILES)
	cp $(PRECOMPILED_PDF_FILES) ./
	touch .copy_assets

%.pdf: $(TEX_PATH)/%.tex
	pdflatex $(PDFLATEX_FLAGS) $<
	# the second pass for \ref
	pdflatex $(PDFLATEX_FLAGS) $<

build_dir:
	mkdir -p $(BUILD_DIR)

clean:
	rm *.pdf *.log *.aux *.thm .copy_assets