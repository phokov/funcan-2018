\documentclass[12pt]{article}
\usepackage[paperwidth=323pt, paperheight=480pt, margin=5pt]{geometry}

\usepackage{cmap}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english,russian]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathrsfs}
\usepackage{mathabx}
\pagenumbering{gobble}

\begin{document}

\begin{center}
\begin{large}
  \textbf{Билет 24.} Теорема Рисса—Фреше об общем виде линейного непрерывного
  функционала в гильбертовом пространстве. Рефлексивность гильбертова
  пространства.
\end{large}
\end{center}

\textbf{Теорема 5.3.1} (Рисс, Фреше). Пусть $\mathscr{H}$ --- гильбертово
пространство. Тогда для любого линейного функционала $f \in \mathscr{H}*$
существует единственный вектор $z(f) \in \mathscr{H}$ такой, что
$f(x) = (x, z(f))$ для любого $x \in \mathscr{H}$, причём $||f|| = ||z(f)||$.
Отображение $z: \mathscr{H}^* \rightarrow \mathscr{H}$ является взаимно
однозначным, изометричным и сопряжённо-линейным, т.е. $z(f + g) = z(f) + z(g)$
и $z(\alpha f) = \overline{\alpha} z(f)$ для всех $f, g \in \mathscr{H}^*$,
$\alpha \in \mathbb{C}$.

\textbf{Доказательство.} Если функционал $f = 0$, то положим $z(0) = 0$.
Рассмотрим произвольный нетривиальный функционал $f \in \mathscr{H}^*$. Тогда
$\ker f$ --- замкнутое подпространство в $\mathscr{H}$, не совпадающее с
$\mathscr{H}$. Так как по теореме 3.2.2. имеем $\mathscr{H} = \ker{f} \oplus
(\ker f)^\perp$ и $\ker{f} \neq \mathscr{H}$, то $(\ker{f})^\perp \neq 0$.
Рассмотрим нетривиальный вектор $y \in (\ker{f})^\perp$. Тогда $f(y) \neq 0$, и
для любого вектора $x \in \mathscr{H}$ выполнено включение $x -
\frac{f(x)}{f(y)} y \in \ker{f}$. Следовательно, получаем равенство $0 = (x -
\frac{f(x)}{f(y)} y, y) = (x , y) - \frac{f(x)}{f(y)} (y, y)$, т.е.
справедливо соотношение $f(x) = (x, \frac{\overline{f(y)}}{(y, y)} y)$.
Определим вектор $z(f) = \frac{\overline{f(y)}}{(y, y)} y$, тогда для любого $x
\in \mathscr{H}$ имеем равенство $f(x) = (x, z(f))$.

Предположим, существует другой вектор $w \in \mathscr{H}$, такой, что

$$f(x) = (x, w) \forall x \in \mathscr{H}$$

Тогда получаем равенство $(x, z(f) - w) = 0$ для любого $x \in \mathscr{H}$.
Следовательно, для вектора $x = z(f) - w \in \mathscr{H}$ получаем

$$(z(f) - w, z(f) - w) = 0$$

т.е. $z(f) = w)$. Таким образом, доказана единственность вектора $z(f)$. В силу
неравенства Коши-Буняковского получаем

$$|f(x)| \leq ||x|| ||z(f)|| \forall x \in \mathscr{H}$$

Следовательно, $||f|| \leq ||z(f)||$. С другой стороны,

$$||z(f)|| = \frac{(z(f), z(f))}{||z(f)||} = \frac{f(z(f))}{||z(f)||} \leq ||f||$$

Следовательно, доказано равенство $||f|| = ||z(f)||$.

Для любых функционалов $f, g \in \mathscr{H}^*$ и любого $x \in \mathscr{H}$
имеем равенства

\begin{multline*}
(f + g)(x) = (x, z(f + g)) = f(x) + g(x) =\\
           = (x, z(f)) + (x, z(g)) = (x, z(f) + z(g))
\end{multline*}

Следовательно, для любого $x \in \mathscr{H}$ получаем

$$(x, z(f + g) - z(f) - z(g)) = 0$$

Тогда для вектора $x = z(f + g) - z(f) - z(g)$ получаем

$$(z(f + g) - z(f) - z(g), z(f + g) - z(f) - z(g)) = 0$$

Т.е. выполнено равенство $z(f + g) = z(f) + z(g)$.

Для любого $f \in \mathscr{H}^*$ и скаляра $\alpha \in \mathbb{C}$ для любого
$x \in \mathscr{H}$ имеем

$$
(\alpha f)(x) = (x, z(\alpha f)) = \alpha f(x) = \alpha (x, z(f)) = (x, \overline{\alpha} z(f))
$$

Следовательно, для любого вектора $x \in \mathscr{H}$ получаем

$$(x, z(\alpha f) - \overline{\alpha} z(f)) = 0$$

Тогда для вектора $x = z(\alpha f) - \overline{\alpha} z(f)$ получаем

$$(z(\alpha f) - \overline{\alpha} z(f), z(\alpha f) - \overline{\alpha} z(f)) = 0$$

т.е. выполнено равенство $z(\alpha f) = \overline{\alpha} z(f)$.

Осталось показать, что образ отображения $z: \mathscr{H}^* \rightarrow
\mathscr{H}$ совпадает с $\mathscr{H}$, т.е. для любого вектора $y \in
\mathscr{H}$ существует линейный функционал $f \in \mathscr{H}^*$, такой, что
$z(f) = y$. Для любого $y \in \mathscr{H}$ рассмотрим линейный функционал $f(x)
= (x, y) \forall \mathscr{H}$. Так как $|f(x)| \leq ||x|| ||y||$, то $||f||
\leq ||y||$. Следовательно, $f \in \mathscr{H}^*$. Так как для функционала $f$
существует единственный вектор $z(f) \in \mathscr{H}$ вида $f(x) = (x, z(f))$
для любого $x \in \mathscr{H}$, то получаем равенство $z(f) = y$, что и
требовалось.

\textbf{Следствие 5.3.1.} Гильбертово пространство $\mathscr{H}$ является
рефлексивным.

\textbf{Доказательство.} Рассмотрим отображение $F: \mathscr{H} \rightarrow
\mathscr{H}^*$ вида $F(x)(f) = f(x)$ для любых $x \in \mathscr{H}$ и $f \in
\mathscr{H}^*$. По определению 5.1.2 требуется доказать, что для любого
функционала $\Phi \in \mathscr{H}^{**}$ существует вектор $y \in \mathscr{H}$
такой что $Fy = \Phi$. Пусть $z: \mathscr{H}^* \rightarrow \mathscr{H}$ ---
сопряжённо-линейное изометрическое взаимно однозначное отображение,
определённое в теореме 5.3.1. Для любого функционала $\Phi \in
\mathscr{H}^{**}$ определим линейный функционал $f = \overline{\Phi \circ
z^{-1}}$, т.е. $f(x) = \overline{\Phi(z^{-1}(x))}$ для любого $x \in
\mathscr{H}$. Тогда имеем неравенство $|f(x)| \leq ||\Phi|| ||z^{-1}(x)|| =
||\Phi|| ||x||$ для любого $x \in \mathscr{H}$, т.е. $||f|| \leq ||Phi||$.
Следовательно, справедливо включение $f \in \mathscr{H}^*$. Определим вектор $y
= z(f)$. Тогда для любого функционала $g \in \mathscr{H}^*$ получаем

\begin{multline*}
(Fy)(g) = g(y) = (z(f), z(g)) = \overline{f(z(g))} =\\
               = \Phi(z^{-1}(z(g))) = \Phi(g)
\end{multline*}

т.е. $Fy = \Phi$, что и требовалось доказать.

\end{document}
